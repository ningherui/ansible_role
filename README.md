##  devops  homework

This is used for testing,and I use the Centos system to test,You must ensure that the server connect the public network and make sure it is consistent with the experimental environment

###  1. Basic architecture

#### container

| name       | local image          | port                 | account | password   |
| ---------- | -------------------- | -------------------- | ------- | ---------- |
| jenkins    | jenkins_test:v_1     | 8080:8080,5000:50000 | root    | test123456 |
| gitlab     | gitlab_test:v_1      | 80:80,443:443,222:22 | root    | test1234   |
| jfrog      | artifactory_test:v_1 | 8081:8081,8082:8082  | admin   | P@ssw0rd   |
| web_server | centos_web_test:v_1  | 2222:22              |         |            |

#### deyploy tools 

| name    | function                                             |
| ------- | :--------------------------------------------------- |
| ansible | automates                                            |
| docker  | install the CI/CD tools based  on the docker-compose |
| jenkins | integration                                          |
| gitlab  | code repository                                      |
| jfrog   | package repository                                   |
| maven   | package tools                                        |

#### CI/CD process

`git push code ------>  jenkins------>  mvn package the code  ------> publish to jfrog  ------> deploy to web_server`

### 2 The operation process

#### 2.1 install ansible on your server 

```bash
yum -y install epel-release
yum -y update
yum -y install ansible 
```

#### 2.2 edit the configure of ansible's hosts

```
[test]
192.168.100.133   #This IP is your remote server ip,and your server need ssh your remote server
```

#### 2.3 ping test

```
[root@NODE1 ~]# ansible test -m ping 
192.168.100.133 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    }, 
    "changed": false, 
    "ping": "pong"
}
```

#### 2.4 download ansible role 

- because of some file size too large,so I upload the role to baidu cloud,please download from baidu cloud

`https://pan.baidu.com/s/1JCZbDWqIXS9HKeO9jv9zSg       code_password:800e`

#### 2.5  the location of role

move the directory(role/devops_test)  to /etc/ansible/role/

```
[root@NODE1 roles]# ll
total 4
drwxr-xr-x 10 root root 118 4月  12 18:59 devops_test
-rw-r--r--  1 root root  48 4月  12 19:08 playbook.yml
```

#### 2.6 install the environemnt

`ansible-playbook /etc/ansible/roles/playbook.yml`

#### 2.7 check the docker container

```
[root@NODE1 ~]# ssh root@192.168.100.133  "docker ps -a"
CONTAINER ID   IMAGE                  COMMAND                  CREATED          STATUS                    PORTS                                                                                                           NAMES
0bfb4cf2cb65   centos_web_test:v_1    "/usr/sbin/sshd -D"      31 minutes ago   Up 31 minutes             0.0.0.0:2222->22/tcp, :::2222->22/tcp                                                                           web_server
366df2f9fb58   artifactory_test:v_1   "/entrypoint-artifac…"   31 minutes ago   Up 31 minutes                                                                                                                             jfrog
ee86a5a4a923   jenkins_test:v_1       "/sbin/tini -- /usr/…"   31 minutes ago   Up 31 minutes                                                                                                                             jenkins
78739b578288   gitlab_test:v_1        "/assets/wrapper"        31 minutes ago   Up 31 minutes (healthy)   0.0.0.0:80->80/tcp, :::80->80/tcp, 0.0.0.0:443->443/tcp, :::443->443/tcp, 0.0.0.0:222->22/tcp, :::222->22/tcp   gitlab
```

#### 2.8 access the web

```
jenkins: http://192.168.100.133:8080/
artifactory: http://192.168.100.133:8082
gitlab: http://192.168.100.133
Jenkinsfile:
http://192.168.100.133/root/jenkins_file
code:
http://192.168.100.133/root/simple-maven-project-with-tests
```

####  2.9 test the CI/CD

build in jenkins

![Image text](https://gitlab.com/ningherui/ansible_role/-/raw/master/image-20210414180446545.png)

console output

```
Started by user root
Obtained Jenkinsfile from git http://www.gitlab_test.com/root/jenkins_file.git
Running in Durability level: MAX_SURVIVABILITY
[Pipeline] Start of Pipeline
[Pipeline] node
Running on Jenkins in /var/jenkins_home/workspace/home_work_mvn_test
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Declarative: Checkout SCM)
[Pipeline] checkout
The recommended git tool is: NONE
using credential 06211290-f04d-48b0-8cd2-f7728d86af4f
 > git rev-parse --is-inside-work-tree # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url http://www.gitlab_test.com/root/jenkins_file.git # timeout=10
Fetching upstream changes from http://www.gitlab_test.com/root/jenkins_file.git
 > git --version # timeout=10
 > git --version # 'git version 2.20.1'
using GIT_ASKPASS to set credentials 
 > git fetch --tags --force --progress -- http://www.gitlab_test.com/root/jenkins_file.git +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git rev-parse refs/remotes/origin/master^{commit} # timeout=10
Checking out Revision 24f9e3062e9a5b2adbb05c0e85cfe4594b21f375 (refs/remotes/origin/master)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f 24f9e3062e9a5b2adbb05c0e85cfe4594b21f375 # timeout=10
Commit message: "Update Jenkinsfile"
 > git rev-list --no-walk 24f9e3062e9a5b2adbb05c0e85cfe4594b21f375 # timeout=10
[Pipeline] }
[Pipeline] // stage
[Pipeline] withEnv
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Build)
[Pipeline] checkout
The recommended git tool is: NONE
using credential 06211290-f04d-48b0-8cd2-f7728d86af4f
 > git rev-parse --is-inside-work-tree # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url http://www.gitlab_test.com/root/simple-maven-project-with-tests.git # timeout=10
Fetching upstream changes from http://www.gitlab_test.com/root/simple-maven-project-with-tests.git
 > git --version # timeout=10
 > git --version # 'git version 2.20.1'
using GIT_ASKPASS to set credentials 
 > git fetch --tags --force --progress -- http://www.gitlab_test.com/root/simple-maven-project-with-tests.git +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git rev-parse refs/remotes/origin/master^{commit} # timeout=10
Checking out Revision b68f46e93e040da0d4c124698088ed53b696af2b (refs/remotes/origin/master)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f b68f46e93e040da0d4c124698088ed53b696af2b # timeout=10
Commit message: "Delete Jenkinsfile"
 > git rev-list --no-walk b68f46e93e040da0d4c124698088ed53b696af2b # timeout=10
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (intergration and test)
[Pipeline] sh
+ mvn -Dmaven.test.failure.ignore=true clean package
[INFO] Scanning for projects...
[INFO] 
[INFO] ----------------< test:simple-maven-project-with-tests >----------------
[INFO] Building simple-maven-project-with-tests 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ simple-maven-project-with-tests ---
[INFO] Deleting /var/jenkins_home/workspace/home_work_mvn_test/target
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ simple-maven-project-with-tests ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /var/jenkins_home/workspace/home_work_mvn_test/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ simple-maven-project-with-tests ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ simple-maven-project-with-tests ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /var/jenkins_home/workspace/home_work_mvn_test/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ simple-maven-project-with-tests ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 3 source files to /var/jenkins_home/workspace/home_work_mvn_test/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ simple-maven-project-with-tests ---
[INFO] Surefire report directory: /var/jenkins_home/workspace/home_work_mvn_test/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running test.OtherTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 1, Time elapsed: 0.007 sec - in test.OtherTest
Running test.SomeTest
Tests run: 6, Failures: 0, Errors: 0, Skipped: 1, Time elapsed: 0.002 sec - in test.SomeTest

Results :

Tests run: 7, Failures: 0, Errors: 0, Skipped: 2

[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ simple-maven-project-with-tests ---
[WARNING] JAR will be empty - no content was marked for inclusion!
[INFO] Building jar: /var/jenkins_home/workspace/home_work_mvn_test/target/simple-maven-project-with-tests-1.0-SNAPSHOT.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  35.214 s
[INFO] Finished at: 2021-04-13T08:52:36+08:00
[INFO] ------------------------------------------------------------------------
Post stage
[Pipeline] junit
Recording test results
[Checks API] No suitable checks publisher found.
[Pipeline] archiveArtifacts
Archiving artifacts
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (deploy)
[Pipeline] sh
+ scp -P 2222 /var/jenkins_home/workspace/home_work_mvn_test/target/simple-maven-project-with-tests-1.0-SNAPSHOT.jar root@centos_web.com:/tmp
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Publish)
[Pipeline] script
[Pipeline] {
[Pipeline] getArtifactoryServer
[Pipeline] newBuildInfo
[Pipeline] artifactoryUpload
[consumer_0] Deploying artifact: http://artifactory_test.com:8082/artifactory/mvn_test/31/simple-maven-project-with-tests-1.0-SNAPSHOT.jar
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Declarative: Post Actions)
[Pipeline] sh
+ echo sent  the build information  to email of administrator
sent  the build information  to email of administrator
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
[withMaven] downstreamPipelineTriggerRunListener - completed in 7930 ms
Finished: SUCCESS

```
- also use the webhook to trigger a build

check jfrog 

![Image text](https://gitlab.com/ningherui/ansible_role/-/raw/master/image-20210414180739906.png)

chekc web_server

```
[root@devops_test_server ~]# docker exec -it web_server ls -l /tmp
total 8
-rw-r--r--. 1 root root 1757 Apr 13 08:53 simple-maven-project-with-tests-1.0-SNAPSHOT.jar
```

The end!
